FROM debian:buster-slim
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -qq update && \
  apt-get -qq install --no-install-recommends ca-certificates && \
  apt-get -qq clean autoclean && apt-get -qq autoremove && rm -rf /var/lib/{apt,dpkg,cache,log}/
COPY bin/bbc /usr/local/bin/
COPY scripts/* /usr/local/bin/
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
