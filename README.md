# BitBucket Cloud Command Line

## Branching Strategy

master - All releases are created from this branch by the build pipeline.
develop - All beta versions are created from this branch by the build pipeline.

The master and develop branches are restricted, and you must issue pull requests
 to merge code into them subject to review and approval by the default reviewers.

## Quick Links
 
* [BitBucket Cloud API](https://developer.atlassian.com/bitbucket/api/2/reference/)

