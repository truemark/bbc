FROM amazon/aws-cli:latest
COPY bin/bbc /usr/local/bin/
COPY scripts/* /usr/local/bin/
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
