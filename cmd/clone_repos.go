package cmd

import (
	"bytes"
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/truemark/bbc/internal/bitbucket"
	"github.com/truemark/bbc/internal/config"
	"github.com/truemark/bbc/internal/files"
	"github.com/truemark/bbc/internal/slices"
	"io/fs"
	"os"
	"os/exec"
	"sort"
	"strings"
)

func isGitRepo(info fs.FileInfo, path string) bool {
	return info.IsDir() && files.DirExists(path, ".git")
}

// Returns the names of all directories in the given directory that contain a .git directory
func gitDirs() []string {
	dirs := files.List(func(info fs.FileInfo, path string) bool {
		return !isGitRepo(info, path)
	}, "./")
	names := make([]string, len(dirs))
	for i, d := range dirs {
		names[i] = d.Name()
	}
	return names
}

func gitClone(url string, dir string) {
	cmdstr := fmt.Sprintf("git clone %s %s", url, dir)
	println(cmdstr)
	cmd := exec.Command("sh", "-c", cmdstr)
	cmdOutput := &bytes.Buffer{}
	cmd.Stdout = cmdOutput
	err := cmd.Run()
	if err != nil {
		os.Stderr.WriteString(err.Error())
	}
	fmt.Print(string(cmdOutput.Bytes()))
}

func cloneRepo(cmd *cobra.Command, repo *bitbucket.Repository) {
	https, err := cmd.Flags().GetBool("https")
	checkFatal(err)
	host, err := cmd.Flags().GetString("host")
	checkFatal(err)
	url := repo.CloneUrl("ssh")
	if https {
		url = repo.CloneUrl("https")
	}
	if host != "" {
		url = strings.Replace(url, "bitbucket.org", host, 1)
	}
	slug := repo.Slug()
	gitClone(url, slug)
}

func cloneRepos(cmd *cobra.Command, repos *bitbucket.Repositories) {
	repos.Sort()
	raw, _ := cmd.Flags().GetBool("raw")
	if raw {
		ra := make([]map[string]interface{}, len(repos.Values))
		for i, v := range repos.Values {
			ra[i] = v.Raw
		}
		printJson(cmd, ra)
	} else {
		dirs := gitDirs()
		for _, repo := range repos.Values {
			slug := repo.Slug()
			dirs = slices.RemoveString(dirs, slug, -1)
			if !files.Exists(slug) {
				cloneRepo(cmd, repo)
			} else {
				println(fmt.Sprintf("%s already exists", slug))
			}
		}
		missing, _ := cmd.Flags().GetBool("missing")
		if missing {
			sort.Strings(dirs)
			for _, s := range dirs {
				println(fmt.Sprintf("%s exists locally and is missing in workspace", s))
			}
		}
	}
}

var cloneReposCmd = &cobra.Command{
	Use:   "clone-repos [workspace] [path]",
	Short: "Clone repositories",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {

		// Get workspace
		parts := strings.Split(args[0], "/")
		c := config.GetBitBucketClient()
		w, err := c.Workspace(parts[0])
		checkFatal(err)

		// Check working directory
		path := args[1]
		err = os.Chdir(path)
		checkFatal(err)

		if log.Info().Enabled() {
			wd, _ := os.Getwd()
			log.Info().Msgf("working directory is %s", wd)
		}



		// Get repositories and make magic
		rs, err := w.Repositories()
		checkFatal(err)
		cloneRepos(cmd, rs)
	},
}

func init() {
	rootCmd.AddCommand(cloneReposCmd)
	cloneReposCmd.Flags().Bool("https", false, "use https to clone repositories")
	cloneReposCmd.Flags().String("host", "", "replace bitbucket.org with the given hostname")
	cloneReposCmd.Flags().Bool("missing", false, "print repos that exist locally but not remotely")
}
