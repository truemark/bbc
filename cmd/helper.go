package cmd

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/truemark/bbc/internal/bitbucket"
	"os"
)

func printJson(cmd *cobra.Command, o interface{}) {
	pretty, _ := cmd.Flags().GetBool("pretty")
	if pretty {
		bitbucket.JsonPrintPretty(o)
	} else {
		bitbucket.JsonPrint(o)
	}
}

func checkFatal(err error) {
	if err != nil {
		log.Fatal().Err(err).Send()
		os.Exit(1)
	}
}

func fatal(format string, a ...interface{}) {
	log.Fatal().Err(fmt.Errorf(format, a...))
	os.Exit(1)
}
