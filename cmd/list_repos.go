package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/truemark/bbc/internal/bitbucket"
	"github.com/truemark/bbc/internal/config"
	"strings"
)

func printRepos(cmd *cobra.Command, repos *bitbucket.Repositories) {
	repos.Sort()
	raw, _ := cmd.Flags().GetBool("raw")
	if raw {
		ra := make([]map[string]interface{}, len(repos.Values))
		for i, v := range repos.Values {
			ra[i] = v.Raw
		}
		printJson(cmd, ra)
	} else {
		c, err := cmd.Flags().GetString("clone")
		checkFatal(err)
		for _, v := range repos.Values {
			if c != "" {
				fmt.Println(v.CloneUrl(c))
			} else {
				fmt.Println(v.Name())
			}

		}
	}
}

var listReposCmd = &cobra.Command{
	Use:   "list-repos [workspace]",
	Short: "List repositories",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		parts := strings.Split(args[0], "/")
		c := config.GetBitBucketClient()
		w, err := c.Workspace(parts[0])
		checkFatal(err)
		rs, err := w.Repositories()
		checkFatal(err)
		printRepos(cmd, rs)
	},
}

func init() {
	rootCmd.AddCommand(listReposCmd)
	listReposCmd.Flags().String("clone", "", "list repositories as clone URLs")
}
