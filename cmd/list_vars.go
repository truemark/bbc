package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"github.com/truemark/bbc/internal/bitbucket"
	"github.com/truemark/bbc/internal/config"
	"strings"
)

func printVars(cmd *cobra.Command, vars *bitbucket.Variables) {
	vars.Sort()
	raw, _ := cmd.Flags().GetBool("raw")
	if raw {
		ra := make([]map[string]interface{}, len(vars.Values))
		for i, v := range vars.Values {
			ra[i] = v.Raw
		}
		printJson(cmd, ra)
	} else {
		for _, v := range vars.Values {
			fmt.Println(v.Key())
		}
	}
}

var listVarsCmd = &cobra.Command{
	Use:   "list-vars [path]",
	Short: "List variables",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		parts := strings.Split(args[0], "/")
		c := config.GetBitBucketClient()
		w, err := c.Workspace(parts[0])
		checkFatal(err)
		if len(parts) == 1 {
			vars, err := w.Variables()
			checkFatal(err)
			printVars(cmd, vars)
			return
		}

		r, err := w.Repository(parts[1])
		checkFatal(err)
		if len(parts) == 2 {
			vars, err := r.Variables()
			checkFatal(err)
			printVars(cmd, vars)
			return
		}

		envs, err := r.Environments()
		checkFatal(err)
		env := envs.Find(parts[2])
		if env == nil {
			fatal("environment %s not found", parts[2])
			return
		}
		vars, err := env.Variables()
		checkFatal(err)
		printVars(cmd, vars)
	},
}

func init() {
	rootCmd.AddCommand(listVarsCmd)
}
