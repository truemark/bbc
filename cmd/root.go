package cmd

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"

	//"github.com/rs/zerolog"
	//"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"os"
	//"path/filepath"
	//"runtime"
)

var cfgFile string
var debug, trace, info bool

var rootCmd = &cobra.Command{
	Use:   "bbc",
	Short: "BitBucket Cloud CLI",
	Long:  `BitBucket Cloud Command Line`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initLogging, initConfig)

	// Global flags and configuration settings
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "enable debug logging")
	rootCmd.PersistentFlags().BoolVar(&trace, "trace", false, "enable trace logging")
	rootCmd.PersistentFlags().BoolVar(&info, "info", false, "enable info logging")
	rootCmd.PersistentFlags().BoolP("raw", "", false, "print raw json")
	rootCmd.PersistentFlags().BoolP("pretty", "", false, "pretty print output")
}

func initLogging() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	level := zerolog.WarnLevel
	if info {
		level = zerolog.InfoLevel
	}
	if trace {
		level = zerolog.TraceLevel
	}
	if debug {
		level = zerolog.DebugLevel
	}
	zerolog.SetGlobalLevel(level)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	log.Debug().
		Str("logLevel", zerolog.GlobalLevel().String()).
		Msg("Global logging configured")
}

func initConfig() {
	viper.SetEnvPrefix("BBC")
	viper.AutomaticEnv()
}
