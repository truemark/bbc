package cmd

import (
	"bufio"
	"github.com/spf13/cobra"
	"github.com/truemark/bbc/internal/bitbucket"
	"github.com/truemark/bbc/internal/config"
	"os"

	"strings"
)

func getValueFromStdIn() string {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		return scanner.Text()
	}
	fatal("error reading from stdin")
	return ""
}

func setVar(vars *bitbucket.Variables, name string, value string, secured bool) {
	v := vars.Find(name)
	var err error
	if v == nil { // Create
		if vars.Environment != nil {
			_, err = vars.Environment.CreateVariable(name, value, secured)
		} else if vars.Repository != nil {
			_, err = vars.Repository.CreateVariable(name, value, secured)
		} else {
			_, err = vars.Workspace.CreateVariable(name, value, secured)
		}
	} else { // Update
		err = v.SetValue(value).SetSecured(secured).Save()
	}
	checkFatal(err)
}

var setVarCmd = &cobra.Command{
	Use:   "set-var [path]",
	Short: "Set variable",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 2 {
			fatal("more than 2 arguments provided")
		}
		parts := strings.Split(args[0], "/")

		if len(parts) < 2 || len(parts) > 4 {
			fatal("invalid path")
		}

		// Strip off variable name from path
		name := parts[len(parts)-1]
		parts = parts[0 : len(parts)-1]

		var vars *bitbucket.Variables

		c := config.GetBitBucketClient()
		w, err := c.Workspace(parts[0])
		checkFatal(err)

		if len(parts) == 1 {
			vars, err = w.Variables()
			checkFatal(err)
		} else if len(parts) == 2 {
			r, err := w.Repository(parts[1])
			checkFatal(err)
			vars, err = r.Variables()
			checkFatal(err)
		} else {
			r, err := w.Repository(parts[1])
			checkFatal(err)
			envs, err := r.Environments()
			checkFatal(err)
			env := envs.Find(parts[2])
			if env == nil {
				fatal("environment %s not found", parts[2])
				return
			}
			vars, err = env.Variables()
		}

		var val string
		if len(args) == 2 { // Value entered in CLI
			val = args[1]
		} else { // Get value from stdin
			val = getValueFromStdIn()
		}

		secured, err := cmd.Flags().GetBool("secured")
		checkFatal(err)
		setVar(vars, name, val, secured)
	},
}

func init() {
	rootCmd.AddCommand(setVarCmd)
	setVarCmd.Flags().BoolP("secured", "s", false, "make variable secure")
}
