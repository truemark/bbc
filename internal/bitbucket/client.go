package bitbucket

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
)

const AccessTokenUrl = "https://bitbucket.org/site/oauth2/access_token"
const BaseUrl = "https://api.bitbucket.org/2.0"
const UserAgent = "bbc" // TODO Introspect to get version and add later

func JsonPrint(v interface{}) {
	data, _ := json.Marshal(v)
	fmt.Println(string(data))
}

func JsonPrintPretty(v interface{}) {
	data, _ := json.MarshalIndent(v, "", "  ")
	fmt.Println(string(data))
}

func CheckStatusCode(req *http.Request, res *http.Response, expected int) error {
	if res.StatusCode != expected {
		return fmt.Errorf("expected status code %v and received %v from %v to %v",
			expected, res.StatusCode, req.Method, req.URL)
	}
	return nil
}

func (c *Client) HttpGet(url string, v interface{}) error {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", c.AuthorizationHeader)
	req.Header.Set("User-Agent", UserAgent)
	log.Debug().
		Interface("url", url).
		Str("method", req.Method).
		Msg("sending HTTP request")
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	log.Debug().
		Int("statusCode", res.StatusCode).
		Str("url", url).
		RawJSON("payload", data).
		Msg("received HTTP response")
	err = CheckStatusCode(req, res, 200)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(data, v); err != nil {
		return err
	}
	return res.Body.Close()
}

func (c *Client) HttpGetForJson(url string) (map[string]interface{}, error) {
	var v map[string]interface{}
	err := c.HttpGet(url, &v)
	return v, err
}

func (c *Client) HttpSendJson(url string, method string, data interface{}, v interface{}) error {
	jdata, err := json.Marshal(data)
	if err != nil {
		return err
	}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(jdata))
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", c.AuthorizationHeader)
	req.Header.Set("User-Agent", UserAgent)
	req.Header.Set("Content-Type", "application/json")
	log.Debug().
		Interface("url", url).
		Str("method", req.Method).
		Msg("sending HTTP request")
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	rdata, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	log.Debug().
		Int("statusCode", res.StatusCode).
		Str("url", url).
		RawJSON("payload", rdata).
		Msg("received HTTP response")
	if method == "POST" {
		err = CheckStatusCode(req, res, 201)
		if err != nil {
			return err
		}
	} else {
		err = CheckStatusCode(req, res, 200)
		if err != nil {
			return err
		}
	}
	if err = json.Unmarshal(rdata, v); err != nil {
		return err
	}
	return res.Body.Close()
}

func (c *Client) HttpPostForJson(url string, data interface{}) (map[string]interface{}, error) {
	var v map[string]interface{}
	err := c.HttpSendJson(url, "POST", data, &v)
	return v, err
}

func (c *Client) HttpPutForJson(url string, data interface{}) (map[string]interface{}, error) {
	var v map[string]interface{}
	err := c.HttpSendJson(url, "PUT", data, &v)
	return v, err
}

func (c *Client) HttpDelete(url string) error {
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}
	req.Header.Set("Authorization", c.AuthorizationHeader)
	req.Header.Set("User-Agent", UserAgent)
	log.Debug().
		Interface("url", url).
		Str("method", req.Method).
		Msg("sending HTTP request")
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}
	log.Debug().
		Int("statusCode", res.StatusCode).
		Str("url", url).
		RawJSON("payload", data).
		Msg("received HTTP response")
	err = CheckStatusCode(req, res, 204)
	if err != nil {
		return err
	}
	return res.Body.Close()
}

type Client struct {
	AuthorizationHeader string
}

func NewClient() *Client {
	return &Client{}
}

type AccessTokenResponse struct {
	Scopes       string `json:"scopes"`
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	TokenType    string `json:"token_type"`
	State        string `json:"state"`
	RefreshToken string `json:"refresh_token"`
}

func (c *Client) getAccessToken(clientId string, secret string) (*AccessTokenResponse, error) {
	values := url.Values{}
	values.Set("grant_type", "client_credentials")
	encoded := values.Encode()
	req, err := http.NewRequest("POST", AccessTokenUrl, strings.NewReader(encoded))
	if err != nil {
		return nil, err
	}
	req.SetBasicAuth(clientId, secret)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(encoded)))
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	err = CheckStatusCode(req, res, 200)
	if err != nil {
		return nil, err
	}
	err = res.Body.Close()
	if err != nil {
		return nil, err
	}
	var atr AccessTokenResponse
	if err := json.Unmarshal(data, &atr); err != nil {
		return nil, err
	}
	return &atr, nil
}

func (c *Client) UseAppPassword(user string, pass string) {
	c.AuthorizationHeader = "Basic " + base64.StdEncoding.EncodeToString([]byte(user+":"+pass))
}

func (c *Client) UseBearerToken(token string) {
	c.AuthorizationHeader = "Bearer " + token
}

func (c *Client) UseClientIdSecret(clientId string, secret string) error {
	token, err := c.getAccessToken(clientId, secret)
	if err != nil {
		return err
	}
	c.UseBearerToken(token.AccessToken)
	return nil
	// TODO In the future we want to refresh the token when it expires
}

// https://developer.atlassian.com/bitbucket/api/2/reference/meta/pagination
type Page struct {
	Page     int                      `json:"page"`     // page number of results
	PageLen  int                      `json:"pagelen"`  // number of objects in page
	Size     int                      `json:"size"`     // total objects in response
	Next     string                   `json:"next"`     // next page
	Previous string                   `json:"previous"` // previous page
	Values   []map[string]interface{} `json:"values"`   // objects in page
}

type Workspace struct {
	Client *Client
	Raw    map[string]interface{}
}

func NewWorkspace(c *Client, r map[string]interface{}) *Workspace {
	return &Workspace{
		Client: c,
		Raw:    r,
	}
}

func (w *Workspace) Name() string {
	return w.Raw["name"].(string)
}

func (w *Workspace) Uuid() string {
	return w.Raw["uuid"].(string)
}

func (w *Workspace) Slug() string {
	return w.Raw["slug"].(string)
}

type Workspaces struct {
	Client *Client
	Values []*Workspace
}

func (wp *Workspaces) Append(wps []*Workspace) {
	wp.Values = append(wp.Values, wps...)
}

func (wp *Workspaces) Sort() {
	sort.Slice(wp.Values, func(i, j int) bool {
		return strings.ToLower(wp.Values[i].Name()) < strings.ToLower(wp.Values[j].Name())
	})
}

func (wp *Workspaces) Size() int {
	return len(wp.Values)
}

func NewWorkspaces(c *Client, w []*Workspace) *Workspaces {
	return &Workspaces{
		Client: c,
		Values: w,
	}
}

type WorkspacePage struct {
	Client     *Client
	Page       *Page
	Workspaces *Workspaces
}

func NewWorkspacePage(c *Client, p *Page) *WorkspacePage {
	var wps []*Workspace
	for _, val := range p.Values {
		wps = append(wps, NewWorkspace(c, val))
	}
	return &WorkspacePage{
		Client:     c,
		Page:       p,
		Workspaces: NewWorkspaces(c, wps),
	}
}

func (wp *WorkspacePage) GoNext() (*WorkspacePage, error) {
	if wp.Page.Next == "" {
		return nil, fmt.Errorf("no next page")
	}
	var p Page
	err := wp.Client.HttpGet(wp.Page.Next, &p)
	if err != nil {
		return nil, err
	}
	return NewWorkspacePage(wp.Client, &p), nil
}

func (wp *WorkspacePage) GoPrevious() (*WorkspacePage, error) {
	if wp.Page.Previous == "" {
		return nil, fmt.Errorf("no previous page")
	}
	var p Page
	err := wp.Client.HttpGet(wp.Page.Previous, &p)
	if err != nil {
		return nil, err
	}
	return NewWorkspacePage(wp.Client, &p), nil
}

func (c *Client) WorkspacesPage() (*WorkspacePage, error) {
	u := fmt.Sprintf("%s/workspaces", BaseUrl)
	var p Page
	err := c.HttpGet(u, &p)
	if err != nil {
		return nil, err
	}
	return NewWorkspacePage(c, &p), nil
}

func (c *Client) Workspaces() (*Workspaces, error) {
	page, err := c.WorkspacesPage()
	if err != nil {
		return nil, err
	}
	wps := page.Workspaces
	for ok := page.Page.Next != ""; ok; ok = page.Page.Next != "" {
		page, err = page.GoNext()
		if err != nil {
			return nil, err
		}
		wps.Append(page.Workspaces.Values)
	}
	return wps, nil
}

func (c *Client) Workspace(name string) (*Workspace, error) {
	u := fmt.Sprintf("%s/workspaces/%s", BaseUrl, name)
	v, err := c.HttpGetForJson(u)
	if err != nil {
		return nil, err
	}
	return NewWorkspace(c, v), nil
}

type Repository struct {
	Client *Client
	Raw    map[string]interface{}
}

func NewRepository(c *Client, r map[string]interface{}) *Repository {
	return &Repository{
		Client: c,
		Raw:    r,
	}
}

func (r *Repository) Name() string {
	return r.Raw["name"].(string)
}

func (r *Repository) Uuid() string {
	return r.Raw["uuid"].(string)
}

func (r *Repository) Slug() string {
	return r.Raw["slug"].(string)
}

func (r *Repository) CloneUrl(t string) string {
	l := r.Raw["links"].(map[string]interface{})
	a := l["clone"].([]interface{})
	for _, v := range a {
		x := v.(map[string]interface{})
		name := x["name"].(string)
		if name == t {
			return x["href"].(string)
		}
	}
	return ""
}

func (r *Repository) Workspace() *Workspace {
	return NewWorkspace(r.Client, r.Raw["workspace"].(map[string]interface{}))
}

type Repositories struct {
	Client *Client
	Values []*Repository
}

func (rp *Repositories) Append(rps []*Repository) {
	rp.Values = append(rp.Values, rps...)
}

func (rp *Repositories) Sort() {
	sort.Slice(rp.Values, func(i, j int) bool {
		return strings.ToLower(rp.Values[i].Name()) < strings.ToLower(rp.Values[j].Name())
	})
}

func (rp *Repositories) Size() int {
	return len(rp.Values)
}

func NewRepositories(c *Client, w []*Repository) *Repositories {
	return &Repositories{
		Client: c,
		Values: w,
	}
}

type RepositoryPage struct {
	Client       *Client
	Page         *Page
	Repositories *Repositories
}

func NewRepositoryPage(c *Client, p *Page) *RepositoryPage {
	var rps []*Repository
	for _, val := range p.Values {
		rps = append(rps, NewRepository(c, val))
	}
	return &RepositoryPage{
		Client:       c,
		Page:         p,
		Repositories: NewRepositories(c, rps),
	}
}

func (rp *RepositoryPage) GoNext() (*RepositoryPage, error) {
	if rp.Page.Next == "" {
		return nil, fmt.Errorf("no next page")
	}
	var p Page
	err := rp.Client.HttpGet(rp.Page.Next, &p)
	if err != nil {
		return nil, err
	}
	return NewRepositoryPage(rp.Client, &p), nil
}

func (rp *RepositoryPage) GoPrevious() (*RepositoryPage, error) {
	if rp.Page.Next == "" {
		return nil, fmt.Errorf("no previous page")
	}
	var p Page
	err := rp.Client.HttpGet(rp.Page.Previous, &p)
	if err != nil {
		return nil, err
	}
	return NewRepositoryPage(rp.Client, &p), nil
}

func (w *Workspace) RepositoryPage() (*RepositoryPage, error) {
	u := fmt.Sprintf("%s/repositories/%s", BaseUrl, w.Slug())
	var p Page
	err := w.Client.HttpGet(u, &p)
	if err != nil {
		return nil, err
	}
	return NewRepositoryPage(w.Client, &p), nil
}

func (w *Workspace) Repositories() (*Repositories, error) {
	page, err := w.RepositoryPage()
	if err != nil {
		return nil, err
	}
	rps := page.Repositories
	for ok := page.Page.Next != ""; ok; ok = page.Page.Next != "" {
		page, err = page.GoNext()
		if err != nil {
			return nil, err
		}
		rps.Append(page.Repositories.Values)
	}
	return rps, nil
}

func (w *Workspace) Repository(name string) (*Repository, error) {
	u := fmt.Sprintf("%s/repositories/%s/%s", BaseUrl, w.Slug(), name)
	v, err := w.Client.HttpGetForJson(u)
	if err != nil {
		return nil, err
	}
	return NewRepository(w.Client, v), nil
}

type Environment struct {
	Repository *Repository
	Raw        map[string]interface{}
}

func NewEnvironment(r *Repository, raw map[string]interface{}) *Environment {
	return &Environment{
		Repository: r,
		Raw:        raw,
	}
}

func (e *Environment) Name() string {
	return e.Raw["name"].(string)
}

func (e *Environment) Uuid() string {
	return e.Raw["uuid"].(string)
}

type Environments struct {
	Repository *Repository
	Values     []*Environment
}

func (es *Environments) Append(envs []*Environment) {
	es.Values = append(es.Values, envs...)
}

func (es *Environments) Sort() {
	sort.Slice(es.Values, func(i, j int) bool {
		return strings.ToLower(es.Values[i].Name()) < strings.ToLower(es.Values[j].Name())
	})
}

func (es *Environments) Size() int {
	return len(es.Values)
}

func (es *Environments) Find(name string) *Environment {
	for _, env := range es.Values {
		if strings.ToLower(env.Name()) == strings.ToLower(name) {
			return env
		}
	}
	return nil
}

func NewEnvironments(r *Repository, e []*Environment) *Environments {
	return &Environments{
		Repository: r,
		Values:     e,
	}
}

type EnvironmentPage struct {
	Repository   *Repository
	Page         *Page
	Environments *Environments
}

func NewEnvironmentPage(r *Repository, p *Page) *EnvironmentPage {
	var eps []*Environment
	for _, val := range p.Values {
		eps = append(eps, NewEnvironment(r, val))
	}
	return &EnvironmentPage{
		Repository:   r,
		Page:         p,
		Environments: NewEnvironments(r, eps),
	}
}

func (r *Repository) Environments() (*Environments, error) {
	u := fmt.Sprintf("%s/repositories/%s/%s/environments/", BaseUrl, r.Workspace().Slug(), r.Name())
	var p Page
	err := r.Client.HttpGet(u, &p)
	if err != nil {
		return nil, err
	}
	page := NewEnvironmentPage(r, &p)
	return page.Environments, nil
}

type Variable struct {
	Workspace   *Workspace
	Repository  *Repository
	Environment *Environment
	Raw         map[string]interface{}
}

func NewVariable(w *Workspace, r *Repository, e *Environment, raw map[string]interface{}) *Variable {
	return &Variable{
		Workspace:   w,
		Repository:  r,
		Environment: e,
		Raw:         raw,
	}
}

func (v *Variable) Key() string {
	return v.Raw["key"].(string)
}

func (v *Variable) SetKey(key string) *Variable {
	v.Raw["key"] = key
	return v
}

func (v *Variable) Uuid() string {
	if val, ok := v.Raw["uuid"]; ok {
		return val.(string)
	}
	return ""
}

func (v *Variable) SetUuid(uuid string) *Variable {
	v.Raw["uuid"] = uuid
	return v
}

func (v *Variable) Value() string {
	if val, ok := v.Raw["value"]; ok {
		return val.(string)
	}
	return ""
}

func (v *Variable) SetValue(value string) *Variable {
	v.Raw["value"] = value
	return v
}

func (v *Variable) Secured() bool {
	return v.Raw["secured"].(bool)
}

func (v *Variable) SetSecured(secured bool) *Variable {
	v.Raw["secured"] = secured
	return v
}

func variableBaseUrl(w *Workspace, r *Repository, e *Environment) string {
	if w != nil && r != nil && e != nil {
		return fmt.Sprintf("%s/repositories/%s/%s/deployments_config/environments/%s/variables",
			BaseUrl, w.Slug(), r.Slug(), e.Uuid())
	}
	if w != nil && r != nil {
		return fmt.Sprintf("%s/repositories/%s/%s/pipelines_config/variables/",
			BaseUrl, w.Slug(), r.Slug())
	}
	if w != nil {
		return fmt.Sprintf("%s/workspaces/%s/pipelines-config/variables",
			BaseUrl, w.Slug())
	}
	return ""
}

func (v *Variable) Save() error {
	u := variableBaseUrl(v.Workspace, v.Repository, v.Environment)
	if v.Uuid() == "" { // Create
		raw, err := v.Workspace.Client.HttpPostForJson(u, v.Raw)
		if err != nil {
			return err
		}
		v.Raw = raw
	} else { // Update
		// BitBucket decided some endpoints have to have a / and some do not
		if strings.HasSuffix(u, "/") {
			u = fmt.Sprintf("%s%s", u, v.Uuid())
		} else {
			u = fmt.Sprintf("%s/%s", u, v.Uuid())
		}
		raw, err := v.Workspace.Client.HttpPutForJson(u, v.Raw)
		if err != nil {
			return err
		}
		v.Raw = raw
	}
	return nil
}

func (v *Variable) Refresh() error {

	if v.Environment != nil {
		// TODO BitBucket doesn't have a GET call for environment variables like the others, so for now we noop
		return nil
	}

	if v.Uuid() == "" {
		return fmt.Errorf("variable uuid is empty")
	}
	var u = variableBaseUrl(v.Workspace, v.Repository, v.Environment)
	// BitBucket decided some endpoints have to have a / and some do not
	if strings.HasSuffix(u, "/") {
		u = fmt.Sprintf("%s%s", u, v.Uuid())
	} else {
		u = fmt.Sprintf("%s/%s", u, v.Uuid())
	}
	raw, err := v.Workspace.Client.HttpGetForJson(u)
	if err != nil {
		return err
	}
	v.Raw = raw
	return nil
}

func (v *Variable) Delete() error {
	if v.Uuid() == "" {
		return fmt.Errorf("variable uuid is empty")
	}
	u := variableBaseUrl(v.Workspace, v.Repository, v.Environment)
	// BitBucket decided some endpoints have to have a / and some do not
	if strings.HasSuffix(u, "/") {
		u = fmt.Sprintf("%s%s", u, v.Uuid())
	} else {
		u = fmt.Sprintf("%s/%s", u, v.Uuid())
	}
	err := v.Workspace.Client.HttpDelete(u)
	if err != nil {
		return err
	}
	v.SetUuid("") // clear out UUID
	return nil
}

type Variables struct {
	Workspace   *Workspace
	Repository  *Repository
	Environment *Environment
	Values      []*Variable
}

func (vrs *Variables) Append(vars []*Variable) {
	vrs.Values = append(vrs.Values, vars...)
}

func (vrs *Variables) Sort() {
	sort.Slice(vrs.Values, func(i, j int) bool {
		return strings.ToLower(vrs.Values[i].Key()) < strings.ToLower(vrs.Values[j].Key())
	})
}

func (vrs *Variables) Size() int {
	return len(vrs.Values)
}

func NewVariables(w *Workspace, r *Repository, e *Environment, v []*Variable) *Variables {
	return &Variables{
		Workspace:   w,
		Repository:  r,
		Environment: e,
		Values:      v,
	}
}

func (vrs *Variables) Find(key string) *Variable {
	for _, v := range vrs.Values {
		if strings.ToLower(v.Key()) == strings.ToLower(key) {
			return v
		}
	}
	return nil
}

type VariablePage struct {
	Workspace   *Workspace
	Repository  *Repository
	Environment *Environment
	Page        *Page
	Variables   *Variables
}

func NewVariablePage(w *Workspace, r *Repository, e *Environment, p *Page) *VariablePage {
	var vars []*Variable
	for _, val := range p.Values {
		vars = append(vars, NewVariable(w, r, e, val))
	}
	return &VariablePage{
		Workspace:   w,
		Repository:  r,
		Environment: e,
		Page:        p,
		Variables:   NewVariables(w, r, e, vars),
	}
}

func (vp *VariablePage) GoNext() (*VariablePage, error) {
	if vp.Page.Next == "" {
		return nil, fmt.Errorf("no next page")
	}
	var p Page
	err := vp.Workspace.Client.HttpGet(vp.Page.Next, &p)
	if err != nil {
		return nil, err
	}
	return NewVariablePage(vp.Workspace, vp.Repository, vp.Environment, &p), nil
}

func (vp *VariablePage) GoPrevious() (*VariablePage, error) {
	if vp.Page.Next == "" {
		return nil, fmt.Errorf("no previous page")
	}
	var p Page
	err := vp.Workspace.Client.HttpGet(vp.Page.Previous, &p)
	if err != nil {
		return nil, err
	}
	return NewVariablePage(vp.Workspace, vp.Repository, vp.Environment, &p), nil
}

func variablePage(w *Workspace, r *Repository, e *Environment) (*VariablePage, error) {
	u := variableBaseUrl(w, r, e)
	c := w.Client
	var p Page
	err := c.HttpGet(u, &p)
	if err != nil {
		return nil, err
	}
	return NewVariablePage(w, r, e, &p), nil
}

func (e *Environment) VariablePage() (*VariablePage, error) {
	return variablePage(e.Repository.Workspace(), e.Repository, e)
}

func (r *Repository) VariablePage() (*VariablePage, error) {
	return variablePage(r.Workspace(), r, nil)
}

func (w *Workspace) VariablePage() (*VariablePage, error) {
	return variablePage(w, nil, nil)
}

func variables(w *Workspace, r *Repository, e *Environment) (*Variables, error) {
	page, err := variablePage(w, r, e)
	if err != nil {
		return nil, err
	}
	vrs := page.Variables
	for ok := page.Page.Next != ""; ok; ok = page.Page.Next != "" {
		page, err = page.GoNext()
		if err != nil {
			return nil, err
		}
		vrs.Append(page.Variables.Values)
	}
	return vrs, nil
}

func (e *Environment) Variables() (*Variables, error) {
	return variables(e.Repository.Workspace(), e.Repository, e)
}

func (r *Repository) Variables() (*Variables, error) {
	return variables(r.Workspace(), r, nil)
}

func (w *Workspace) Variables() (*Variables, error) {
	return variables(w, nil, nil)
}

func (w *Workspace) CreateVariable(key string, value string, secured bool) (*Variable, error) {
	v := NewVariable(w, nil, nil, make(map[string]interface{})).
		SetKey(key).SetValue(value).SetSecured(secured)
	err := v.Save()
	if err != nil {
		return nil, err
	}
	return v, nil
}

func (r *Repository) CreateVariable(key string, value string, secured bool) (*Variable, error) {
	v := NewVariable(r.Workspace(), r, nil, make(map[string]interface{})).
		SetKey(key).SetValue(value).SetSecured(secured)
	err := v.Save()
	if err != nil {
		return nil, err
	}
	return v, nil
}

func (e *Environment) CreateVariable(key string, value string, secured bool) (*Variable, error) {
	v := NewVariable(e.Repository.Workspace(), e.Repository, e, make(map[string]interface{})).
		SetKey(key).SetValue(value).SetSecured(secured)
	err := v.Save()
	if err != nil {
		return nil, err
	}
	return v, nil
}
