package bitbucket

import (
	"encoding/json"
	"github.com/rs/zerolog/log"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"testing"
)

type TestDataAuth struct {
	User string `json:"user"`
	Pass string `json:"pass"`
}

type TestData struct {
	Auth        TestDataAuth `json:"auth"`
	Skip        []string     `json:"skip"`
	Workspace   string       `json:"workspace"`
	Repository  string       `json:"repository"`
	Environment string       `json:"environment"`
	Variable    string       `json:"variable"`
}

func Skipped(name string) bool {
	for _, s := range tdata().Skip {
		if s == name {
			log.Info().Str("func", name).Msg("test skipped")
			return true
		}
	}
	return false
}

func NotSkipped(name string) bool {
	return !Skipped(name)
}

var _testData *TestData
var _client *Client
var _workspace *Workspace
var _repository *Repository
var _environment *Environment

func tdata() *TestData {
	wd, _ := os.Getwd()
	log.Info().Str("path", wd).Msg("working directory")

	if _testData == nil {
		file, err := os.Open("../../test.json")
		if err != nil {
			log.Panic().Err(err).Msg("failed to open test.json")
		}
		defer file.Close()
		data, err := ioutil.ReadAll(file)
		if err != nil {
			log.Panic().Err(err).Msg("failed to read test.json")
		}
		err = json.Unmarshal(data, &_testData)
		if err != nil {
			log.Panic().Err(err).Msg("failed to parse test.json")
		}
	}
	return _testData
}

func tclient() *Client {
	if _client == nil {
		testData := tdata()
		_client = NewClient()
		_client.UseAppPassword(testData.Auth.User, testData.Auth.Pass)
	}
	return _client
}

func tworkspace(t *testing.T) *Workspace {
	if _workspace == nil {
		c := tclient()
		w, err := c.Workspace(tdata().Workspace)
		assert.NoError(t, err, "failed to get workspace %s", tdata().Workspace)
		_workspace = w
	}
	return _workspace
}

func trepository(t *testing.T) *Repository {
	if _repository == nil {
		w := tworkspace(t)
		if w != nil {
			r, err := w.Repository(tdata().Repository)
			assert.NoError(t, err, "failed to get repository %s", tdata().Repository)
			_repository = r
		}
	}
	return _repository
}

func tenvironment(t *testing.T) *Environment {
	if _environment == nil {
		r := trepository(t)
		if r != nil {
			envs, err := r.Environments()
			if assert.NoError(t, err) {
				return envs.Find(tdata().Environment)
			}
		}
	}
	return _environment
}

func TestGetWorkspaces(t *testing.T) {
	if NotSkipped("TestGetWorkspaces") {
		c := tclient()
		wps, err := c.Workspaces()
		if assert.NoError(t, err) && assert.NotNil(t, wps) && assert.Greater(t, wps.Size(), 0) {
			wps.Sort()
			for _, w := range wps.Values {
				log.Debug().
					Str("name", w.Name()).
					Str("uuid", w.Uuid()).
					Msg("workspace")
			}
		}
	}
}

func TestGetWorkspaceRepositories(t *testing.T) {
	if NotSkipped("TestGetWorkspaceRepositories") {
		w := tworkspace(t)
		if w != nil {
			log.Debug().
				Str("name", w.Name()).
				Str("uuid", w.Uuid()).
				Msg("workspace")
			rps, err := w.Repositories()
			if assert.NoError(t, err) && assert.NotNil(t, rps) && assert.Greater(t, rps.Size(), 0) {
				rps.Sort()
				for _, r := range rps.Values {
					log.Debug().
						Str("name", r.Name()).
						Str("uuid", r.Uuid()).
						Msg("repository")
				}
			}
		}
	}
}

func TestGetRepositoryEnvironments(t *testing.T) {
	if NotSkipped("TestGetRepositoryEnvironments") {
		r := trepository(t)
		if r != nil {
			envs, err := r.Environments()
			if assert.NoError(t, err) && assert.NotNil(t, envs) && assert.Greater(t, envs.Size(), 0) {
				for _, e := range envs.Values {
					log.Debug().
						Str("name", e.Name()).
						Str("uuid", e.Uuid()).
						Msg("repository")
				}
			}
		}
	}
}

func TestGetEnvironmentVariables(t *testing.T) {
	if NotSkipped("TestGetEnvironmentVariables") {
		r := trepository(t)
		if r != nil {
			envs, err := r.Environments()
			if assert.NoError(t, err) && assert.NotNil(t, envs) {
				e := envs.Find(tdata().Environment)
				if assert.NotNil(t, e) {
					vars, err := e.Variables()
					if assert.NoError(t, err) {
						for _, v := range vars.Values {
							log.Debug().
								Str("key", v.Key()).
								Str("uuid", v.Uuid()).
								Msg("variable")
						}
					}
				}
			}
		}
	}
}

func testVariableUpdates(t *testing.T, v *Variable) {
	// Update unsecure variable
	v.SetValue("TEST1")
	err := v.Save()
	if !assert.NoError(t, err) {
		return
	}
	assert.NoError(t, err)
	assert.Equal(t, v.Value(), "TEST1")

	// Update to secure variable
	v.SetSecured(true)
	err = v.Save()
	if !assert.NoError(t, err) {
		return
	}
	err = v.Refresh()
	assert.NoError(t, err)
	assert.Equal(t, v.Value(), "")

	// Delete variable
	err = v.Delete()
	if !assert.NoError(t, err) {
		return
	}
	assert.NoError(t, err)
	assert.Equal(t, v.Uuid(), "")
}

func TestWorkspaceVariables(t *testing.T) {
	if NotSkipped("TestWorkspaceVariables") {
		w := tworkspace(t)
		if w != nil {
			vars, err := w.Variables()
			if assert.NoError(t, err) {
				v := vars.Find(tdata().Variable)
				if assert.Nil(t, v, "delete variable %s from workspace %s", tdata().Variable, w.Name()) {

					// Create unsecure variable
					v, err = w.CreateVariable(tdata().Variable, "TEST0", false)
					if !assert.NoError(t, err) {
						return
					}
					assert.NoError(t, err)
					assert.Equal(t, v.Value(), "TEST0")

					testVariableUpdates(t, v)
				}
			}
		}
	}
}

func TestRepositoryVariables(t *testing.T) {
	if NotSkipped("TestRepositoryVariables") {
		w := tworkspace(t)
		r := trepository(t)
		if w != nil && r != nil {
			vars, err := r.Variables()
			if assert.NoError(t, err) {
				v := vars.Find(tdata().Variable)
				if assert.Nil(t, v, "delete variable %s from repository %s/%s",
					tdata().Variable, w.Name(), r.Name()) {

					// Create unsecure variable
					v, err = r.CreateVariable(tdata().Variable, "TEST0", false)
					if !assert.NoError(t, err) {
						return
					}
					assert.NoError(t, err)
					assert.Equal(t, v.Value(), "TEST0")

					testVariableUpdates(t, v)
				}
			}
		}
	}
}

func TestEnvironmentVariables(t *testing.T) {
	if NotSkipped("TestEnvironmentVariables") {
		w := tworkspace(t)
		r := trepository(t)
		e := tenvironment(t)
		if w != nil && r != nil && e != nil {
			vars, err := e.Variables()
			if assert.NoError(t, err) {
				v := vars.Find(tdata().Variable)
				if assert.Nil(t, v, "delete variable %s from repository %s/%s in environment %s",
					tdata().Variable, w.Name(), r.Name(), e.Name()) {

					// Create unsecure variable
					v, err = e.CreateVariable(tdata().Variable, "TEST0", false)
					if !assert.NoError(t, err) {
						return
					}
					assert.NoError(t, err)
					assert.Equal(t, v.Value(), "TEST0")

					testVariableUpdates(t, v)
				}
			}
		}
	}
}
