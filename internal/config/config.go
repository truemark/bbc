package config

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/truemark/bbc/internal/bitbucket"
)

func GetBitBucketClient() *bitbucket.Client {
	c := bitbucket.NewClient()
	if viper.IsSet("CLIENT_ID") && viper.IsSet("SECRET") {
		clientId := viper.GetString("CLIENT_ID")
		secret := viper.GetString("SECRET")
		err := c.UseClientIdSecret(clientId, secret)
		if err != nil {
			log.Fatal().Err(err).Send()
		}
	} else if viper.IsSet("USER") && viper.IsSet("PASS") {
		user := viper.GetString("USER")
		pass := viper.GetString("PASS")
		c.UseAppPassword(user, pass)
	} else {
		log.Fatal().Msg("no authentication credentials found")
	}
	return c
}
