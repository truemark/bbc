package files

import (
	"io"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
)

type FileFilter func(fs.FileInfo, string) bool

func CombineFileFilters(filters ...FileFilter) FileFilter {
	return func(info fs.FileInfo, path string) bool {
		for _, filter := range filters {
			if filter(info, path) {
				return true
			}
		}
		return false
	}
}

func List(filter FileFilter, path ...string) []fs.FileInfo {
	var ls []fs.FileInfo
	dir := filepath.Join(path...)
	fileInfos, err := ioutil.ReadDir(dir)
	if err != nil {
		return ls
	}
	for _, fileInfo := range fileInfos {
		filePath := filepath.Join(dir, fileInfo.Name())
		if !filter(fileInfo, filePath) {
			ls = append(ls, fileInfo)
		}
	}
	return ls
}

func Exists(path ...string) bool {
	dir := filepath.Join(path...)
	_, err := os.Stat(dir)
	return err == nil
}

func DirExists(path ...string) bool {
	dir := filepath.Join(path...)
	fileInfo, err := os.Stat(dir)
	if err != nil {
		return false
	}
	return fileInfo.IsDir()
}

func FileExists(path ...string) bool {
	file := filepath.Join(path...)
	fileInfo, err := os.Stat(file)
	if err != nil {
		return false
	}
	return !fileInfo.IsDir()
}

func DirEmpty(path ...string) (bool, error) {
	dir := filepath.Join(path...)
	file, err := os.Open(dir)
	if err != nil {
		return false, err
	}
	defer file.Close()
	_, err = file.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	return false, nil
}
