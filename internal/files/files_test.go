package files

import (
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

var dir string = ""

func setup() {
	if dir == "" {
		d, err := ioutil.TempDir(os.TempDir(), "go_files_test")
		if err != nil {
			log.Fatal().Err(err).Send()
		}
		dir = d
	}
	log.Debug().Str("path", dir).Msg("created temp directory")
}

func cleanup() {
	if dir != "" {
		err := os.RemoveAll(dir)
		if err != nil {
			log.Fatal().Err(err).Send()
		}
		log.Debug().Str("path", dir).Msg("deleted temp directory")
	}
	dir = ""
}

func TestExists(t *testing.T) {
	setup()
	defer cleanup()
	file := filepath.Join(dir, "empty.txt")
	_, err := os.Create(file)
	if err != nil {
		log.Fatal().Err(err).Send()
	}
	if !Exists(file) {
		t.Fail()
	}
}
