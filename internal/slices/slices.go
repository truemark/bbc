package slices

type Match func(i1 interface{}, i2 interface{}) bool

func Remove(match Match, a []interface{}, x interface{}, n int) []interface{} {
	for i, v := range a {
		if match(v, x) {
			a = append(a[:i], a[i+1:]...)
			n--
			if n == 0 {
				break
			}
		}
	}
	return a
}

// Removes a string from a slice of strings n number of times.
// Set n to a negative number to remove all matches.
func RemoveString(a []string, s string, n int) []string {
	for i, v := range a {
		if v == s {
			a = append(a[:i], a[i+1:]...)
			n--
			if n == 0 {
				break
			}
		}
	}
	return a
}
