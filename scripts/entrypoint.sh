#!/usr/bin/env bash

init_array_var() {
    local array_var=${1}
    local count_var=${array_var}_COUNT
    for (( i = 0; i < ${!count_var:=0}; i++ ))
    do
      eval ${array_var}[$i]='$'${array_var}_${i}
    done
}

BBC_ARG_COUNT=$(set | grep -c "^BBC_ARG")
if [[ "${BBC_ARG_COUNT}" != 0 ]]; then
  init_array_var 'BBC_ARG'
  bbc "${BBC_ARG[@]}"
elif [[ -n "${BBC_SCRIPT}" ]]; then
  ${BBC_SCRIPT}
else
  bbc "${@}"
fi
