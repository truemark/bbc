#!/usr/bin/env bash

###############################################################################
# AWS Access Key Rotation
###############################################################################

set -euo pipefail

[[ -z "${BBC_USER+z}" ]] && [[ -z "${BBC_CLIENT_ID+z}" ]] && echo "BBC_USER and BBC_PASS or BBC_CLIENT_ID and BBC_SECRET is required" && exit 1
[[ -n "${BBC_USER+z}" ]] && [[ -z "${BBC_PASS}" ]] && echo "BBC_PASS is required" && exit 1
[[ -n "${BBC_CLIENT_ID+z}" ]] && [[ -z "${BBC_SECRET}" ]] && echo "BBC_SECRET is required" && exit 1
[[ -z "${BBC_PATH+z}" ]] && echo "BBC_PATH is required" && exit 1
[[ -z "${BBC_AWS_IAM_USER_NAME+z}" ]] && echo "BBC_AWS_IAM_USER_NAME is required" && exit 1
[[ -z "${BBC_AWS_ACCESS_KEY_ID_VAR+z}" ]] && BBC_AWS_ACCESS_KEY_ID_VAR="AWS_ACCESS_KEY_ID"
[[ -z "${BBC_AWS_SECRET_ACCESS_KEY_VAR+z}" ]] && BBC_AWS_SECRET_ACCESS_KEY_VAR="AWS_SECRET_ACCESS_KEY"

count=$(aws iam list-access-keys --user-name "${BBC_AWS_IAM_USER_NAME}" --query 'length(AccessKeyMetadata)')
if [[ "${count}" == "2" ]]; then
  oldest=$(aws iam list-access-keys --user-name "${BBC_AWS_IAM_USER_NAME}" --query 'sort_by(AccessKeyMetadata, &CreateDate)[:1].AccessKeyId' --output text)
  aws iam update-access-key --user-name "${BBC_AWS_IAM_USER_NAME}" --access-key-id "${oldest}" --status "Inactive"
  aws iam delete-access-key --user-name "${BBC_AWS_IAM_USER_NAME}" --access-key-id "${oldest}"
fi
creds=($(aws iam create-access-key --user-name "${BBC_AWS_IAM_USER_NAME}" --query "AccessKey.{AccessKeyId: AccessKeyId SecretAccessKey: SecretAccessKey}" --output text))

for var in ${!BBC_PATH*}; do
  declare "bbcp=${!var}"
  echo "updating keys for ${bbcp}"
  echo "${creds[0]}" | bbc set-var "${bbcp}/${BBC_AWS_ACCESS_KEY_ID_VAR}"
  echo "${creds[1]}" | bbc set-var "${bbcp}/${BBC_AWS_SECRET_ACCESS_KEY_VAR}" -s
done
